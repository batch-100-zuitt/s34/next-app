import { Jumbotron, Row, Col } from 'react-bootstrap';
import Link from 'next/link';
import PropTypes from 'prop-types';


export default function Banner({dataProp}){
    //destructure the data prop into its properties
    const { title, content, destination, label } = dataProp;

    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>{title}</h1>
                    <p>{content}</p>
                    <Link href={destination}><a>{label}</a></Link>
                </Jumbotron>
            </Col>
        </Row>
    )

}


Banner.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        destination: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
    })
}
