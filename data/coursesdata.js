export default [
	{
		id: "wdc001",
		name: "HTML",
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
		price: 45000,
		onOffer: true,
		start_date: "Feb 20,2021",
		end_date: "August 19, 2021"
	},
	{

		id: "wdc002",
		name: "CSS",
		description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
		price: 50000,
		onOffer: true,
		start_date: "Feb 20, 2021",
		end_date: "August 19, 2021"
	},
	{

		id: "wdc003",
		name: "React JS",
		description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
		price: 5000,
		onOffer: false,
		start_date: "March 20, 2021",
		end_date: "June 20, 2021"
	}

]