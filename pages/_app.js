import React, { useState, useEffect } from 'react';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';


import NavBar from '../components/NavBar';
import { Container } from 'react-bootstrap';

//import Context Provider
import { UserProvider } from '../UserContext';


export default function MyApp({ Component, pageProps }) {
  //state hook for user state, define here for global scope
  const [user, setUser] = useState({
    email: null,
    isAdmin: null
  })

  //localStorage can only be accessed after this component has been rendered
  useEffect(() => {
    setUser({
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
    })
  }, [])

  //effect hook for testing the setUser() functionality
  useEffect(() => {
    console.log(user.email);
  }, [user.email])

  //function for clearing local storage upon logout
  const unsetUser = () => {
    localStorage.clear();

    //set the user global scope in the context provider  to have its
    //email and isAdmin set to null
    setUser({
      email: null,
      isAdmin: null
    });
  }
  
  return (
      <React.Fragment>
        <UserProvider value={{user, setUser, unsetUser}}>
          <NavBar />
          <Container>
              <Component {...pageProps} />
          </Container>
        </UserProvider>
      </React.Fragment>
  );
  
}


