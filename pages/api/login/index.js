import { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../../UserContext';
import usersData from '../../data/usersdata';
import Router from 'next/router';



    export default function index() {
 
     const { setUser } = useContext(UserContext);

        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');

        function authenticate(e) {

        
            e.preventDefault();

            const match = usersData.find(user => {
            return (user.email === email && user.password === password);
         })

         if(match){
            localStorage.setItem('email', email);
            localStorage.setItem('isAdmin', match.isAdmin);

            setUser({
              
                email: match.email,
                isAdmin: match.isAdmin
            });

            Router.push('/courses');
        } else {
            console.log("Authentication failed, no match found.")
        }

         
            setEmail('');
            setPassword('');
    }

       
    return (
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password"
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
            <Button className="bg-primary" type="submit">Submit</Button>
        </Form>
    )
}
